package com.webservice.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.webservice.app"})
public class WebServiceAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebServiceAppApplication.class, args);
	}

}
