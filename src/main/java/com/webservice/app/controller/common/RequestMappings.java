package com.webservice.app.controller.common;

public interface RequestMappings {

	public static String RETRIEVE_ALL_EMPLOYEES = "/retrieveAllEmployees";

	public static String CREATE_EMPLOYEE = "/createEmployees";

	public static String DELETE_EMPLOYEE_BY_ID = "/deleteById/{emp_id}";

	public static String UPDATE_EMPLOYEE_BY_ID = "/updateById/{emp_id}";
	
	public static String RETRIEVE_EMPLOYEE_BY_ID = "/getById/{emp_id}";
}
