package com.webservice.app.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.webservice.app.controller.common.RequestMappings;
import com.webservice.app.domain.CreateEmployeeRequest;
import com.webservice.app.domain.CreateEmployeeResponse;
import com.webservice.app.domain.DeleteEmployeeResponse;
import com.webservice.app.domain.EmployeeResponse;
import com.webservice.app.domain.RetrieveAllEmployeeResponse;
import com.webservice.app.domain.UpdateEmployeeRequest;
import com.webservice.app.service.EmployeeService;

@RestController
public class EmployeeController {

	final static Logger logger = Logger.getLogger(EmployeeController.class);

	@Autowired
	private EmployeeService employeeService;

	/*
	 * Retrieve all employees
	 */
	@RequestMapping(value = RequestMappings.RETRIEVE_ALL_EMPLOYEES, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<RetrieveAllEmployeeResponse> retrieveAllEmployees() {

		logger.info("Inside retrive all employees endpoint");

		RetrieveAllEmployeeResponse allEmployeeResponse = null;

		try {
			allEmployeeResponse = employeeService.retrieveAllEmployees();

		} catch (Exception e) {
			return new ResponseEntity<RetrieveAllEmployeeResponse>(allEmployeeResponse, HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<RetrieveAllEmployeeResponse>(allEmployeeResponse, HttpStatus.OK);
	}

	/*
	 * Create employee
	 */
	@RequestMapping(value = RequestMappings.CREATE_EMPLOYEE, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<CreateEmployeeResponse> createEmployee(
			@RequestBody CreateEmployeeRequest request) {

		logger.info("Inside create employee endpoint");

		CreateEmployeeResponse createEmployeeResponse = null;

		try { 
			createEmployeeResponse = employeeService.createEmployee(request);

		} catch (Exception e) {
			return new ResponseEntity<CreateEmployeeResponse>(createEmployeeResponse, HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<CreateEmployeeResponse>(createEmployeeResponse, HttpStatus.CREATED);
	}

	/*
	 * Delete employee by id
	 */
	@RequestMapping(value = RequestMappings.DELETE_EMPLOYEE_BY_ID, method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<DeleteEmployeeResponse> deleteEmployeeById(
			@PathVariable(value = "emp_id", required = true) String employeeId) {

		logger.info("Inside delete employee by id endpoint");

		DeleteEmployeeResponse deleteEmployeeResponse = null;

		try {
			deleteEmployeeResponse = employeeService.deleteEmployeeById(employeeId);

		} catch (Exception e) {
			return new ResponseEntity<DeleteEmployeeResponse>(deleteEmployeeResponse, HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<DeleteEmployeeResponse>(deleteEmployeeResponse, HttpStatus.OK);
	}

	/*
	 * Update employee by id
	 */
	@RequestMapping(value = RequestMappings.UPDATE_EMPLOYEE_BY_ID, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<EmployeeResponse> updateEmployeeById(@RequestBody UpdateEmployeeRequest request,
			@PathVariable(value = "emp_id", required = true) String employeeId) {

		logger.info("Inside update employee by id endpoint");

		EmployeeResponse employeeResponse = null;

		try {
			employeeResponse = employeeService.updateEmployeeById(employeeId, request);

		} catch (Exception e) {
			return new ResponseEntity<EmployeeResponse>(employeeResponse, HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<EmployeeResponse>(employeeResponse, HttpStatus.OK);
	}

	/*
	 * Retrieve employee by id
	 */
	@RequestMapping(value = RequestMappings.RETRIEVE_EMPLOYEE_BY_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<EmployeeResponse> retrieveEmployeeById(
			@PathVariable(value = "emp_id", required = true) String employeeId) {

		logger.info("Inside retrieve employee by id endpoint");

		EmployeeResponse employeeResponse = null;

		try {
			employeeResponse = employeeService.retrieveEmployeeById(employeeId);

		} catch (Exception e) {
			return new ResponseEntity<EmployeeResponse>(employeeResponse, HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<EmployeeResponse>(employeeResponse, HttpStatus.OK);
	}
}
