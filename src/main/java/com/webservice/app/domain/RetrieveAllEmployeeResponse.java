package com.webservice.app.domain;

import java.util.ArrayList;
import java.util.List;

public class RetrieveAllEmployeeResponse {

	private List<EmployeeResponse> employeeResponses;

	public RetrieveAllEmployeeResponse() {
	}

	public List<EmployeeResponse> getEmployees() {
		if (employeeResponses == null) {
			employeeResponses = new ArrayList<EmployeeResponse>();
		}
		return employeeResponses;
	}

	public void setEmployees(List<EmployeeResponse> employeeResponses) {
		this.employeeResponses = employeeResponses;
	}
}
