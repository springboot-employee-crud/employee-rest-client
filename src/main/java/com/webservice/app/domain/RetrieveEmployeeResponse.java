package com.webservice.app.domain;

import java.util.ArrayList;
import java.util.List;

import ch.qos.logback.core.status.Status;

public class RetrieveEmployeeResponse {

	private Status status;

	private List<Employee> users = new ArrayList<Employee>();

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public List<Employee> getUsers() {
		return users;
	}

	public void setUsers(List<Employee> users) {
		this.users = users;
	}

}
