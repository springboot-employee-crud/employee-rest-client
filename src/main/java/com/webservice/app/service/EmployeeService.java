package com.webservice.app.service;

import com.webservice.app.domain.CreateEmployeeRequest;
import com.webservice.app.domain.CreateEmployeeResponse;
import com.webservice.app.domain.DeleteEmployeeResponse;
import com.webservice.app.domain.EmployeeResponse;
import com.webservice.app.domain.RetrieveAllEmployeeResponse;
import com.webservice.app.domain.UpdateEmployeeRequest;

public interface EmployeeService {

	RetrieveAllEmployeeResponse retrieveAllEmployees();

	CreateEmployeeResponse createEmployee(CreateEmployeeRequest request);

	DeleteEmployeeResponse deleteEmployeeById(String employeeId);

	EmployeeResponse updateEmployeeById(String employeeId, UpdateEmployeeRequest request);

	EmployeeResponse retrieveEmployeeById(String employeeId);

}
