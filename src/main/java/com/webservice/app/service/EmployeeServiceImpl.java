package com.webservice.app.service;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;

import com.webservice.app.domain.CreateEmployeeRequest;
import com.webservice.app.domain.CreateEmployeeResponse;
import com.webservice.app.domain.DeleteEmployeeResponse;
import com.webservice.app.domain.EmployeeResponse;
import com.webservice.app.domain.RetrieveAllEmployeeResponse;
import com.webservice.app.domain.UpdateEmployeeRequest;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	final static Logger logger = Logger.getLogger(EmployeeServiceImpl.class);

	@Autowired
	RestTemplate restTemplate;

	@Value("${api.key}")
	private String apiKey;

	@Value("${emp.get.all.url}")
	private String retrieveAllEmployeesUrl;

	@Value("${emp.create.url}")
	private String createEmployeeUrl;

	@Value("${emp.delete.by.id.url}")
	private String deleteEmployeeByIdUrl;

	@Value("${emp.update.by.id.url}")
	private String updateEmployeeByIdUrl;

	@Value("${emp.get.by.id.url}")
	private String retrieveEmployeeByIdUrl;

	@Override
	public RetrieveAllEmployeeResponse retrieveAllEmployees() {
		logger.info("Inside retrieve all employees service");

		ResponseEntity<RetrieveAllEmployeeResponse> responseEntity = null;

		try {

			HttpEntity<String> httpEntity = new HttpEntity<String>(setHeaders());

			responseEntity = restTemplate.exchange(retrieveAllEmployeesUrl, HttpMethod.GET, httpEntity,
					RetrieveAllEmployeeResponse.class);

//					exchange(GET_ALL_USERS_API,HttpMethod.GET, httpEntity, RetrieveAllEmployeeResponse.class);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new RuntimeException("Error while retrieving data from API");
		}

		logger.info("Successfully retrieved employees from API");
		return responseEntity.getBody();

//		return restTemplate.getForObject(GET_ALL_USERS_API, RetrieveAllEmployeeResponse.class);

	}

	@Override
	public CreateEmployeeResponse createEmployee(CreateEmployeeRequest request) {
		logger.info("Inside create employee service");

		Assert.hasText(request.getName(), "Employee name is required");
		Assert.hasText(request.getAddress(), "Employee address is required");
		Assert.notNull(request.getSalary(), "Employee salary is required");
		Assert.hasText(request.getEmail(), "Employee email is required");
		Assert.hasText(request.getPhone(), "Employee phone is required");

		ResponseEntity<CreateEmployeeResponse> responseEntity = null;

		try {

			HttpEntity<CreateEmployeeRequest> httpEntity = new HttpEntity<CreateEmployeeRequest>(request, setHeaders());

			responseEntity = restTemplate.exchange(createEmployeeUrl, HttpMethod.POST, httpEntity,
					CreateEmployeeResponse.class, httpEntity);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new RuntimeException("Error while creating employee from API");
		}
		logger.info("Successfully created employee from API");
		return responseEntity.getBody();
	}

	@Override
	public DeleteEmployeeResponse deleteEmployeeById(String employeeId) {
		logger.info("Inside delete employee by id service");

		Assert.hasText(employeeId, "Employee id is required");
		ResponseEntity<DeleteEmployeeResponse> responseEntity = null;

		try {

			HttpEntity<String> httpEntity = new HttpEntity<String>(setHeaders());

			responseEntity = restTemplate.exchange(deleteEmployeeByIdUrl + employeeId, HttpMethod.DELETE, httpEntity,
					DeleteEmployeeResponse.class);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new RuntimeException("Error while deleting employee from API");
		}
		logger.info("Successfully deleted employee from API");
		return responseEntity.getBody();
	}

	@Override
	public EmployeeResponse updateEmployeeById(String employeeId, UpdateEmployeeRequest request) {
		logger.info("Inside update employee by id service");

		Assert.hasText(employeeId, "Employee id is required");
		Assert.hasText(request.getName(), "Employee name is required");
		Assert.hasText(request.getAddress(), "Employee address is required");
		Assert.notNull(request.getSalary(), "Employee salary is required");
		Assert.hasText(request.getEmail(), "Employee email is required");
		Assert.hasText(request.getPhone(), "Employee phone is required");

		ResponseEntity<EmployeeResponse> responseEntity = null;

		try {

			HttpEntity<UpdateEmployeeRequest> httpEntity = new HttpEntity<UpdateEmployeeRequest>(request, setHeaders());

			responseEntity = restTemplate.exchange(updateEmployeeByIdUrl + employeeId, HttpMethod.PUT, httpEntity,
					EmployeeResponse.class);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new RuntimeException("Error while updating employee from API");
		}
		logger.info("Successfully updated employee from API");
		return responseEntity.getBody();
	}

	@Override
	public EmployeeResponse retrieveEmployeeById(String employeeId) {
		logger.info("Inside retrieve employee by id service");

		Assert.hasText(employeeId, "Employee id is required");
		ResponseEntity<EmployeeResponse> responseEntity = null;

		try {

			HttpEntity<String> httpEntity = new HttpEntity<String>(setHeaders());

			responseEntity = restTemplate.exchange(retrieveEmployeeByIdUrl + employeeId, HttpMethod.GET, httpEntity,
					EmployeeResponse.class);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new RuntimeException("Error while retrieving employee from API");
		}
		logger.info("Successfully retrieved employee from API");
		return responseEntity.getBody();
	}

	private HttpHeaders setHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.set("api-key", apiKey);

		return headers;
	}
}
